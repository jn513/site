---
title: "Circumventing Unfair Telegram Blocking by the State"
date: 2023-04-28T11:04:44-03:00
draft: false
categories: ["segurança"]
tags: ["vpn", "proxy", "telegram", "freedom"]
authors: ["Julio Nunes Avelar"]
chat_id: "contornarbroqueiotelegramen"
---

# Circumventing Unfair Telegram Blocking by the State

On April 26, 2023, Telegram was again blocked in Brazil, an unconstitutional act that harms users' rights. In this post, we will discuss how to circumvent these blocks and help maintain the privacy and security of Telegram users.

## How is Telegram Blocked?

Telegram is usually blocked by internet service providers (ISPs) by court order or government order. This means that when a user tries to access Telegram, the ISP can block access to the Telegram network, preventing users from connecting.

The methods used to block access to Telegram may vary, but they usually involve blocking specific IP addresses used by the app or blocking certain domains used by Telegram. These techniques can be applied at the ISP level or at a broader network level, such as national blocking.

## Circumventing the Block

There are two main ways to circumvent the block, using VPN or Proxy, each with its advantages and disadvantages.

### Using VPN

VPN stands for "Virtual Private Network." It is a technology that allows you to create a secure and private connection between a device (such as a computer or smartphone) and the internet, through an intermediary server.

In simple terms, a VPN works as a secure tunnel that protects the user's communication, encrypting the data sent and received over the internet. This helps protect the user's privacy and ensure that their personal information, such as passwords and bank data, is not intercepted by third parties.

Pros:

- Any connection, even in other protocols, is encapsulated, offering good anonymity;
- It works for any application;
- Prevents "Man-in-the-Middle" attacks;
- Perfect for public connections, such as restaurant Wi-Fi;
- Circumvents blocks, such as the one Telegram is facing in Brazil.

Cons:

- The entire internet becomes slower;
- The VPN server has access to everything the user does, which can be risky. Therefore, it is important to use secure VPNs;
- Shared IPs with many clients can make the internet even slower.

Some well-rated VPNs (We recommend doing a thorough search before choosing a VPN for security reasons):

- ExpressVPN;
- NordVPN.
- CyberGhost VPN
- Private Internet Access (PIA)
- ProtonVPN
- VyprVPN
- Windscribe VPN

### Using Proxy

A proxy is a server that acts as an intermediary between a device (such as a computer or smartphone) and the internet. When a user connects to a proxy, the server forwards the user's requests to internet servers and then returns the server's responses back to the user's device. Telegram has the option to use a proxy natively.

Pros:

- Does not affect the speed of the connection in general;
- Does not interfere with other applications;
- Allows the use of Telegram.

Cons:

- Does not offer good anonymity;
- It is possible to identify users who are using a proxy to access Telegram.

To use a proxy in Telegram, it is possible to use some configuration links provided by the application itself that configure the proxy automatically. Below are some proxy options:

[Digital Resistance proxy](tg://proxy?server=proxy.digitalresistance.dog&port=443&secret=ddd41d8cd98f00b204e9800998ecf8427e
)

[Cloudflare UK proxy](tg://proxy?server=cloudflare.com.nokia.com.co.uk.du_yo.want_to.with.this.www.googleso.com.baronia-intheparket.sbs&port=443&secret=ee000000000000000000000000000000006b65746161626f6e6c696e652e636f6d)


![Configure proxy](/imgs/posts/telegram/proxy.png)

#### Tor Proxy

One possibility for a proxy is to use the Tor network as a proxy. To use this feature, you will need to have Tor installed on your computer or mobile device, and the configuration must be done manually.

Configuration:

- Every time you want to use the proxy, you must start Tor on your device.
- Add the proxy to your Telegram and select to use it. On Android devices, go to Settings > Data and Storage > Proxy Configuration. In the desktop version, go to Settings > Advanced > Connection Type.
- Enable the "Use Proxy" option.

![Enable Proxy](/imgs/posts/telegram/ativarproxy.png)

- Configure a new proxy.
- Select the SOCKS5 type.
- Put the server as "localhost" or "127.0.0.1" and the port as "9150".

![Configure Proxy](/imgs/posts/telegram/config.png)

- Save the configuration, and Telegram will attempt to connect to the proxy automatically.

Remember that using a proxy does not guarantee complete anonymity and privacy on the internet. Additionally, be careful when choosing the source of your information about proxies and VPNs, as there are many scams and risks involved. If you know of any other secure proxies or VPNs that have not been listed here, please inform us so that we can update this list and provide more options to our readers.