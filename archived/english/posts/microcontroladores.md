---
title: "Choosing microcontrollers for IoT devices and embedded systems"
date: 2023-04-30T15:27:07-03:00
draft: false
categories: ["hardware", "microcontroladore","embarcados"]
tags: ["esp32","eletronica"]
authors: ["Julio Nunes Avelar"]
chat_id: "microcontroladores-diff"
---

# Choosing microcontrollers for IoT devices and embedded systems

![Microcontrollers](/imgs/posts/microcontroladores/microcontroladores.jpg)

## Summary:

Here is a brief guide to help you find the topics mentioned in this post:

* [Introduction](#introduction)
    * [Internet of Things (IoT)](#internet-of-things-iot)
    * [Embedded Systems](#embedded-systems)
    * [IoT Devices](#iot-devices)
    * [Microcontrollers](#microcontrollers)
* [Choosing the ideal microcontroller](#choosing-the-ideal-microcontroller)
    * [Processing power](#processing-power)
    * [Volatile memory](#volatile-memory)
    * [Non-volatile memory](#non-volatile-memory)
    * [Interconnectivity](#interconnectivity)
    * [Peripherals](#peripherals)
    * [Energy efficiency](#energy-efficiency)
    * [Security](#security)
    * [Cost](#cost)
* [Comparing some microcontrollers](#comparing-some-microcontrollers)

## Introduction

First of all, let's define some important concepts.

### Internet of Things (IoT)

Firstly, let's understand what the Internet of Things (IoT) is. There is no general consensus on the definition of the term, but we can understand it as a set of technologies and protocols that allow digital objects to connect to the communication network and be controlled by it. These "objects" are devices that have computing, communication, and control resources.

![Internet of Things](/imgs/posts/microcontroladores/iot.jpg)

### Embedded Systems

Another important term is "embedded systems." We can define them as electronic systems dedicated to a specific function or set of functions, designed to operate in restricted and often hostile environments, with limitations on resources such as memory, processing, and energy. They are incorporated into larger devices or equipment such as automobiles, medical devices, home appliances, industrial automation equipment, and many others. Embedded systems generally contain a microcontroller or specialized processor, as well as memory, input/output interfaces, and other components necessary to perform their functions.

![Embedded systems](/imgs/posts/microcontroladores/embarcados.jpg)

### IoT Devices

As mentioned earlier, IoT devices are the "objects" of the IoT. They are devices with computing, communication, and control resources, which are usually built with components such as microcontrollers, wireless communication modules, sensors, and actuators. It is important to note that IoT devices are a type of embedded system.

![IoT devices](/imgs/posts/microcontroladores/iot-devices.jpg)

### Microcontrollers

Now that we understand the basic concepts, let's talk about microcontrollers. Microcontrollers are small computers integrated on a single chip (integrated circuit) that contain a CPU (Central Processing Unit), memory, and input/output peripherals.

But how do you choose the ideal microcontroller for your IoT device or embedded system project? Well, that's quite a challenge! Fortunately, in this post, we will explore how to choose the ideal microcontroller for your IoT device or embedded system project, taking into account its characteristics and the challenges of security and privacy in the IoT. With the right information, it is possible to create efficient, reliable, and secure technological solutions for all types of requirements.

## Choosing the ideal microcontroller

There are several factors that should be considered when choosing a microcontroller for your project. Here are some of the most important characteristics:

### Processing power

The microcontroller's processing power is important in determining how fast it can perform the necessary tasks or how many tasks it can perform in parallel. If your project involves real-time data processing or complex tasks involving multiple tasks, it is important to choose a microcontroller with a more powerful CPU, with data such as number of cores and maximum clock speed being important points to consider in this regard.

### Volatile Memory

The microcontroller's volatile memory is composed of memory types such as DRAM (Dynamic Random Access Memory), SRAM (Static Random Access Memory), and PSRAM (Pseudo-Static Random Access Memory), which are used by the CPU to store temporary information during execution in order to perform specific functions. These memories rely on power to function, meaning that when the microcontroller is turned off, all information is lost. The microcontroller uses volatile memory to store data that it needs to access quickly, such as program instructions and data variables. This allows the microcontroller to perform tasks quickly and efficiently. In summary, volatile memory is essential in a microcontroller because it offers a temporary, high-speed storage space for critical data for the execution of its functions. Devices that require a large allocation of memory for data storage or function execution require microcontrollers with a larger amount of this type of memory.

### Non-volatile Memory

Non-volatile memory in a microcontroller is essential for maintaining data that needs to be stored even when the system is not energized. This data includes programs to be executed by the microcontroller, configuration information, files, and others. It is important to note that generally, these types of memories are slower than volatile ones. For applications that require the storage of large programs or many files with the microcontroller, memory capacity should be considered. If only files need to be stored, some microcontrollers offer the option of using external memories.

### Interconnectivity

For IoT devices or embedded systems that require connectivity (Wi-Fi, Bluetooth, Zigbee, LoRa, etc.), it is interesting to choose a microcontroller that already has built-in interconnectivity resources, which can reduce costs and resource consumption.

### Peripherals

Peripherals, such as input and output devices, analog ports, and serial communication protocols, are useful features for a microcontroller. It is important to choose a microcontroller that provides the necessary peripherals for your project natively or that allows the addition of external modules.

### Energy Efficiency

If your device is powered by batteries or solar panels, energy efficiency is crucial to ensure the battery life and proper functioning of the system. It is important to analyze the microcontroller's energy consumption in both continuous operation and hibernation mode.

### Security

One of the major challenges of the Internet of Things (IoT) today is security. This concern covers from the highest to the lowest layer, including microcontrollers. IoT devices store various sensitive data, such as access credentials and closed code parts. Additionally, with remote updates via OTA and wireless communication, it is crucial to have resources such as flash encryption, security boot, and support for encryption in general. Devices that require a minimum level of security and will be used in non-isolated environments must have these essential resources. Therefore, it is crucial to choose a microcontroller that supports these resources.

### Cost

Finally, it is essential to consider the acquisition and production cost of the microcontroller, ensuring that it is within your budget and allowing for large-scale device production.

## Comparing some microcontrollers

| Name | Processor | No. of Cores | Clock | M. Flash | M. RAM (DRAM or SRAM) | Connectivity | Peripherals | GPIOs | ADC | PWM | Operating Voltage | Cost |
|---|---|---|---|---|---|---|---|---|---|---|---|---| 
| Arduino Mega | ATmega2560 | 1 | 16Mhz | 256KB | 8KB | | 1 SPI, 1 I2C, 4 UART | 54 | 16 - 10bits | 15 | 5v | R$76.33 | 
| Arduino Uno | ATmega328 | 1 | 16Mhz | 16KB | 2KB | | 1 SPI, 1 I2C, 1 UART | 14 | 6 - 10bits | 6 | 5v | R$26.31 | 
| Arduino Zero | ARM Cortex M0+ | 1 | 48Mhz | 256KB | 32KB | | 1 SPI, 1 I2C, 2 UART, 2 I2S | 20 | 5 - 12bits, 1 - 10bits | 12 | 3.3v | R$59.00 | 
| ESP-32 | Xtensa LX6 | 2 | 240Mhz | up to 16MB | 520KB | WIFI, BLE | 4 SPI, 2 I2C, 2 I2S, 3 UART, CAN | 36 | 18 -  12bits | 16 | 3.3v | R$12.82 | 
| ESP-32S2 | Xtensa LX7 | 1 | 240Mhz | up to 16MB | 320KB | WIFI | 4 SPI, 2 I2C, 2 I2S, 2 UART, CAM, OTG, LCD | 43 | 20 -  13bits | 8 | 3.3v | R$10.46 | 
| ESP-32S3 | Xtensa LX7 | 2 | 240Mhz | up to 32MB | 512KB | WIFI, BLE | 4 SPI, 2 I2C, 2 I2S, 3 UART, CAM, OTG,  LCD,  USB Serial JTag | 45 | 20 -  13bits | 8 | 3.3v | R$20.03 | 
| ESP-32C3 | 32-bit RiscV | 1 | 160Mhz | up to 16MB | 400KB | WIFI, BLE | 3 SPI, 1 I2C, 1 I2S, 2 UART | 22 | 12 - 12bits | 6 | 3.3v | R$10,51 | 
| ESP-8266 | Xtensa L106 | 1 | 160Mhz | até 16MB | 160KB | WIFI | 4 SPI, 2 I2C, 2 I2S, 2 UART | 17 | 1 - 10bits | 8 | 3.3v | R$07,85 | 
| NRF52810 | ARM Cortex-M4 | 1 | 64Mhz | 192KB | 24KB | BLE | SPI, I2C, UART, PDM | 32 | 8 - 12bits | 4 | 3.3v | R$24,90 | 
| Raspberry Pi PICO | ARM Cortex-M0+ | 2 | 133Mhz | até 16MB | 264KB | | 2 SPI, 2 I2C, 2 UART, CAN, OTG | 30 | 3 - 12bits | 16 | 3.3v | R$23,28 | 
| Raspberry Pi PICO W | ARM Cortex-M0+ | 2 | 133Mhz | até 16MB | 264KB | WIFI | 2 SPI, 2 I2C, 2 UART, CAN, OTG | 30 | 3 - 12bits | 16 | 3.3v | R$53,40 | 

Note: The data in this table was updated on the publication date of this text. If there is any difference or inconsistency, please let us know. It is important to note that the prices of ESP32XX and ESP8266 microcontrollers only take into account the module price, while the prices of other microcontrollers include the complete development board. This is because the other microcontrollers are not only offered as modules, but rather as a complete version with the development board or only the integrated circuit. Additionally, these prices were quoted on Aliexpress and may vary due to import taxes, depending on the current legislation at the time of purchase.

[Click here to access the full table.](https://docs.google.com/spreadsheets/d/1fQaBdGPpOdVy6Vd7BasbjTs7t7Xta5GCtgDcengZ8Jg/edit?usp=sharing)