---
title: "Bzóide: The Origin"
date: 2023-04-27T15:27:13-03:00
draft: false
tags: ["site"]
categories: ["History"]
authors: ["Julio Nunes Avelar"]
chat_id: "genesisen"
---

# 🤖📚 Bzóide: The Origin 🧐

If you're reading this post, you're probably curious about the meaning behind the name of this blog: Bzóide. 🤔 After all, it's quite a "different" name, isn't it?

But don't worry, because we're here to tell you the whole story behind this enigmatic name. 📖

## 🕰️ Historical Context

Firstly, it's important to know that Bzóide has its roots in the State University of Campinas, better known as [UNICAMP](https://ic.unicamp.br/graduacao/engenharia-da-computacao/). 🎓 In this institution, in the Computer Engineering course, there are two concentration areas: Computer Systems (AA) and Industrial Systems and Processes (AB).

Students in the AA area are affectionately called "Azóides," while those in the AB area are known as "Bzóides." 😎

However, there is a sad reality behind this story: most students opt for Computer Systems (AA) instead of Industrial Systems and Processes (AB). 😞 This results in classes with more than 60 Azóides and only about half a dozen Bzóides.

For this reason, there is an urban legend at UNICAMP that Bzóides are just a myth and don't actually exist. 😱 But the truth is that these students are rare and definitely the coolest and friendliest people on campus. So, if you meet a bzoide, take good care of them because they are truly a rarity. 😉

## 🤖🧐 The Meaning of the Name

Now, you may be wondering: "but what does Bzóide mean, anyway?" Well, the answer is simple: it's an abbreviation of the name given to students in the AB area of the Computer Engineering course at UNICAMP. 😁

And that's exactly why we chose this name for our blog. We want to prove to the world that Bzóides exist and are much more than just a simple myth. 😉

## 📚 Conclusion

In summary, the name Bzoide is a tribute to the students in the Industrial Systems and Processes area of the Computer Engineering course at UNICAMP. 🎓 And, as such, it is a representation of the rarity and uniqueness of these students within the university.

We hope that now you understand the meaning behind the name of this blog and stay with us to follow our publications. 🤖📚