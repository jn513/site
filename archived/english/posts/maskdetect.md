---
title: "Facial detection that recognizes whether the person is wearing a mask or not"
date: 2023-04-28T09:42:46-03:00
draft: false
categories: ["machine learning"]
tags: ["python", "OpenCV"]
authors: ["Julio Nunes Avelar"]
chat_id: "maskdetect"
---

# Facial detection that recognizes whether the person is wearing a mask or not

![Example](https://raw.githubusercontent.com/JN513/mask_detect/master/testes/teste.det.jpg)

Hey, dev! Today I'm here to present a very interesting project using Python, aiming to detect people without face masks because, after all, since the beginning of the pandemic caused by the coronavirus, several places have determined the mandatory use of this item. But without further ado, let's go!

## 👋 Introduction

I will show you how to automatically detect who is or isn't wearing a mask in a specific environment. How? With computer vision and machine learning.

## 🤨 Hold on! We need to know about two terms before proceeding on this journey.

👉 **Computer vision** is the process of modeling and replicating human vision using software and hardware. In practice, we have as an example the need to identify whether someone is wearing a face mask or not, in other words, the machine will be replicating human vision and perception of the situation.

👉 **Machine learning** is a subfield of Artificial Intelligence that works with the idea that machines can learn on their own from a huge amount of data. In the example to be presented here, the machine will learn to detect the presence or absence of a mask from a database of images of people wearing masks.

## 🤔 What do I need to know to better understand the subject?

👉 Python programming language;
👉 Pip;
👉 Numpy;
👉 Open CV;

Follow until the end to get study material tips!

## 🤯 What did I use in the project?

First of all, you can access the project through the links below:

👉 Version on [Google Colab](https://colab.research.google.com/drive/145o1e8Z23aKkgBZT8cpcavjVAc8VHskp?authuser=1);
👉 Version in [Python code](https://github.com/JN513/mask_detect);

Now let's go to the dependencies you will need to install:

👉 Version 3.0 or higher of Python;
👉 Libraries:
* Tensor Flow;
* Open CV;
* Numpy;
* Cython;
* Codecov;
* Pytest-cov;
* Pytesseract;
* Tesseract-ocr;
* Wand;
* Sklearn;
* Imutils;
* Matplotlib;
* PIL.

❗️ In the [project repository](https://github.com/JN513/mask_detect), you can find them in the requirements.txt file to download them easily. Just use the following command in the terminal/cmd.

```bash 
pip3 install -r requirements.txt
``` 

## 🦾 Project organization

The project has 3 files:

👉 **Training File:** To train the model, simply run the **treino.py** file. (It's worth noting that the project has a database of 690 images of people with masks and 686 without them. To add an image, simply place it in the "dataset" folder).

👉 **Image Detection File:** To perform the detection process, simply use the detect_image.py file and pass the **-i** argument and the **path of the image**.

Example:

```bash 
python3 detect_image.py -i testes/1.png
```

👉 **Video or Camera Detection File:** To use your device's default camera, simply run the **detect_camera.py** file.

Example:

```bash 
python3 detect_camera.py
```

However, if you want to use an external camera or an electronic device via droidcam, simply put the camera's IP on line 63.

Example:

```bash
captura_video = cv2.VideoCapture("192.168.0.102:4224")
```

Finally, to use a video, simply pass its **path** on the same line mentioned above.

## 📚 Study Time, Dev!

Below are some study sources so you can venture into the amazing world of computer vision and machine learning.

👉 Python:

  * <https://www.python.org/>

👉 Python Open CV:

  * <https://opencv-python-tutroals.readthedocs.io/>

👉 Numpy:

  * <https://numpy.org/>

👉 Tensor FLow: 

  * <https://www.tensorflow.org/>


## Results

### - Loss and Accuracy

![Loss and Accuracy](https://raw.githubusercontent.com/JN513/mask_detect/master/plot.png)

### - Accuracy

![Accuracy](https://raw.githubusercontent.com/JN513/mask_detect/master/plotloss.png)

### - Loss

![Loss](https://raw.githubusercontent.com/JN513/mask_detect/master/plotacc.png)



## Credits:

This project was based on [Gorpo's](https://github.com/gorpo/Face-Recognition-Detector-de-Mascara-Python-Covid-19) project (since I don't know his name, I'll stick with the nickname haha).

Text revision and correction --> [Luisa Manoela Romão Sales](https://neps.academy/user/10702)