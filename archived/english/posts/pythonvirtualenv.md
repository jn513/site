---
title: "Python virtual environments"
date: 2023-04-28T10:24:28-03:00
draft: false
categories: ["utilitarios"]
tags: ["Python", "venv"]
authors: ["Julio Nunes Avelar"]
chat_id: "pythonvirtualenv"
---

# 🐍 Python virtual environments 🐍

Imagine this: you have two Python projects that use the numpy library, but one requires version 1.5 and the other version 1.3. What to do? Install only one version on your computer and sacrifice one of the projects? No need! With Python virtual environments, you can have isolated instances of Python for each project! 🤯

## But wait, what are these virtual environments? 🤔

A Python virtual environment is a directory that contains an isolated Python installation, with the necessary libraries and dependencies for a specific project. They allow you to isolate different projects from each other, ensuring that each one has its own set of libraries and configurations, without interfering with each other. 🤓

## And why should you use a Python virtual environment? Here are some reasons: 🤔

- 👉 Package isolation: you can install specific packages for a project without interfering with other projects or with the global Python installation.
- 👉 Environment consistency: you can ensure that everyone involved in a project is using exactly the same environment, helping to ensure consistency in results.
- 👉 Ease of configuration: when you create a Python virtual environment, it comes with a copy of the Python interpreter and standard libraries, which means you don't have to worry about installing or configuring Python on each machine.
- 👉 Portability: a Python virtual environment can be easily moved from one machine to another, which can be useful if you need to work on different computers.
- 👉 Reproducibility: by using a virtual environment, you can ensure that your code works the same way, regardless of the environment in which it is executed.

## And how do you create a Python virtual environment?

Simple! You can use the built-in `venv` module in Python. Here's how to create a new virtual environment: 🤓

- 1️⃣ Open a terminal or command prompt window.
- 2️⃣ Navigate to the directory where you want to create the virtual environment.
- 3️⃣ Type `python3 -m venv myenv` (replace `myenv` with the name you want to give to your virtual environment).
- 4️⃣ Press Enter to create the virtual environment.

After creating the virtual environment, you can activate it by running the following command: 🚀

On Windows: `myenv\Scripts\activate`

On Unix/MacOS: `source myenv/bin/activate`

Once you activate the virtual environment, you can install any necessary packages using `pip`, without affecting the global Python installation. When you're done working on the project, you can deactivate the virtual environment by running the `deactivate` command. 💻