---
title: "LoRa - A technology where free software can contribute"
date: 2023-04-27T16:14:13-03:00
draft: false
tags: ["LoRa", "LoRaWAN", "LoRa Mesh", "Free software"]
categories: ["Hardware", "LoRa", "Bibliotecas"]
authors: ["Julio Nunes Avelar", "Tiago Silveira Zaparoli"]
chat_id: "lora"
---

# LoRa - A technology where free software can contribute

## Summary:

Here is a brief guide to help you find the topics mentioned in this post:

* [What is LoRa?](#what-is-lora)
* [Why use LoRa?](#why-use-lora)
* [Protocols for LoRa](#protocols-for-lora)
    * [LoRaWAN](#lorawan)
        * [Architecture](#architecture)
        * [Endpoint devices](#endpoint-devices)
        * [Gateways](#gateways)
        * [Device classes](#device-classes)
        * [Security](#security)
        * [Advantages and disadvantages](#advantages-and-disadvantages)
    * [LoRa Mesh](#lora-mesh)
        * [Architecture](#architecture-1)
        * [Security](#security-1)
        * [Advantages and disadvantages](#advantages-and-disadvantages-1)
* [Operating ranges](#operating-ranges)
* [Collaborative networks](#collaborative-networks)
* [Development of LoRa libraries](#development-of-lora-libraries)
* [Development of the LoRa Mesh Protocol](#development-of-the-lora-mesh-protocol)
* [How free software can help spread LoRa](#how-free-software-can-help-spread-lora)
* [Sources](#sources)

## What is LoRa?

LoRa is a low-frequency radio communication technology that allows long-distance communication with low power consumption. Its range varies from 3 to 4 km in urban areas and reaches up to 12 km in rural areas with clear line-of-sight. Its most common use is in IoT (Internet of Things) devices, ranging from monitoring systems such as sensors to actuators, and is used especially in devices with low energy capacity, such as battery-powered devices, devices in hard-to-reach locations, among others. Just for curiosity's sake, the LoRa acronym means Long Range.

## Why use LoRa?

LoRa, like all communication technologies, has its advantages and disadvantages, with its major disadvantage being its low bandwidth. You may wonder why use LoRa instead of technologies such as Wi-Fi, GSM, and Bluetooth, for example. These mentioned technologies have advantages such as ease of use due to the wide available literature, considerable bandwidth capacity, and, in cases such as GSM, broad coverage. The biggest advantage of LoRa over these technologies that justifies its use is the low power consumption and long range without depending on third-party infrastructure (as is the case with GSM). Devices with Wi-Fi and Bluetooth communication have their range limited to about 200 meters at best, and a very high power consumption considering the characteristics of IoT devices, while using GSM provides a very wide range due to the large mobile network coverage currently available, although its consumption is relatively high and dependent on the infrastructure of network operators. These and other gaps favor the use of LoRa over these and other technologies, especially in cases where long range and low consumption need to go hand in hand.


## Protocols for LoRa.

Like other communication technologies, LoRa also has some protocols that run on top of it, such as LoRaWAN and LoRa Mesh.

### LoRaWAN

LoRaWAN is the name of the main protocol used with LoRa. It defines the system architecture and communication parameters using LoRa. Currently, the LoRaWAN protocol is used predominantly by almost all existing LoRa applications. It implements details of operation, security, quality of service, power adjustments, and types of applications used both on the IoT device side and on the server side.

#### Architecture

The LoRaWAN protocol uses a layered topology, with a star topology used for communication between LoRa devices. All LoRa-based devices communicate with a gateway, which acts as a bridge sending information from the network to the devices and vice versa.

#### Endpoints Devices

Endpoints devices are the basic devices in the network such as sensors and actuators. These devices are the outermost layer of the network and use only LoRa for communication.

#### Gateways

Gateways are devices responsible for bridging between endpoints and the rest of the network (routers, servers, etc.). In general, these devices have the ability to communicate by 2 or more means, commonly using LoRa and some other type of communication technology such as LoRa and Wi-Fi or LoRa and Ethernet, for example. A gateway can receive data from thousands of devices and forward them to a network server. The coverage of a gateway depends only on the power of its LoRa modules and the topology of the installation location, with the coverage radius ranging from 2 to 15 km according to these parameters.

#### Device Classes

To meet different demands, LoRaWAN protocol defines three device classes:

1. Class A - Sensors: Bi-directional communication, reception after transmission. (Modules can only receive data in predetermined time windows immediately after transmitting.)
2. Class B - Actuators: Bi-directional communication, with scheduled reception windows.
3. Class C - Bi-directional, with almost continuous data reception: In this class, the module is always ready to receive data from the gateway.

#### Security

The LoRaWAN protocol provides two levels of security. The part of the packet that contains the "payload" is encrypted with AES 128 encryption, and the packet as a whole is signed using AES 128 encryption to ensure the integrity of the sent packet, thus ensuring that the packet was not altered by errors or intentionally.

#### Advantages and disadvantages

LoRaWAN has several advantages, such as high energy efficiency, easy implementation, available literature with plenty of standards. On the other hand, it is a highly centralized protocol, where to have redundancy, you are required to have gateways with overlapping coverage areas. Additionally, for places with terrain with various obstacles, gateways have limited range, which requires spending on repeaters or other gateways.

### LoRa Mesh

Starting with the name Mesh, mesh means a network in which various nodes can communicate with each other. In a mesh network, the more nodes the network has, the greater its range, but the energy consumption of the network devices increases, and the capacity decreases as the nodes begin to have to process and transmit information from other nodes.

#### Architecture

The LoRa Mesh protocol uses a mesh topology, where all devices can send a message to another without necessarily passing through a gateway. To communicate with the server, the network can use a gateway-like device, but more than one device can assume this gateway condition and bridge the internet.



![Mesh topology](/imgs/posts/lora/topologia-malha.jpeg)
Mesh topology


#### Security

The LoRa Mesh protocol does not provide any specific security standards, with this standard being left to the network implementation. For security in the LoRa Mesh network, resources from LoRaWAN can be reused.

#### Advantages and disadvantages

The biggest advantage of the LoRa Mesh network is the high availability and redundancy of the network, as well as its much greater coverage, since the more devices there are in the network, the greater its range. On the other hand, its implementation is much more complex and has much less literature available than LoRaWAN, in addition to its lower energy efficiency, which is something with a great impact for battery-based devices.

## Operating bands.

LoRa operates in frequency bands from 433MHz to 928MHz, with each country having specific regulations. In Brazil, the bands regulated by Anatel are from 902MHz to 907.5MHz and from 915MHz to 928MHz.

![Operating bands](/imgs/posts/lora/faixa-operacao.webp)
LoRa communication bands - Source: Eletrogate

## Collaborative Networks

Currently, there are several projects aimed at allowing the development of LoRa WAN networks at low cost. One way to do this is through collaborative networks where each individual or group of individuals maintains a gateway on their own and allows the free use of this gateway by others, creating a network with broad coverage that anyone can use and where the acquisition and maintenance costs are completely diluted, in addition to not depending on any central entity for the maintenance of these equipment. A major initiative in this area is The Things Network (TTN). The Things Network is a collaborative IoT network on a global scale that provides a low-cost cloud framework for connecting gateways and LoRa-based applications, so every time someone buys or sets up a gateway and connects to TTN, any user on the network can start using that gateway to access the TTN cloud. In this way, a global network is created collaboratively and diluted.

## Development of LoRa Libraries

There are currently several libraries that allow the use of LoRa in its "pure" form or through LoRaWAN, but these libraries have limitations in their implementations for certain architectures, with many microcontrollers not having a native port for it, or when there is a library it is abandoned. The good news is that because there are several ready-made libraries, there is something already done to base it on, only needing to be ported to the native technologies of certain microcontrollers.

## Development of the LoRa Mesh Protocol

LoRa Mesh is a technology that, although very promising, remains little implemented and documented, with much of what is currently done being proprietary, making its development and widespread use by many people difficult. Therefore, there is much to be done, such as the development of standards, documentation, as well as the practical implementation of software to enable the LoRa Mesh standard.


## How open source software can help spread LoRa.

As mentioned in the previous sections, there is still much to be developed for LoRa, from libraries to complete protocols, and one possible solution to solve this problem is the use of open source software, creating completely free libraries and protocols, thus allowing contribution from the entire community to the development and maintenance of these technologies. Before concluding this article, I would like to propose some contributions from the community: currently there is a lack of native libraries for LoRa usage with ESP32 microcontroller families (ESP32, ESP32S2, ESP32C3, ESP32S2, etc) using the native ESP-IDF framework; there is also a lack of native libraries for STM platform, among others; in addition, there are several ports for using other languages and development frameworks that do not have libraries for LoRa usage, such as the Zephyr RTOS and the RUST language on various microcontrollers; finally, I would also like to suggest contributing to the construction of a fully open source LoRa Mesh protocol.

Some of the suggestions mentioned above already have some initiatives in the area, some even of my own authorship. Such as the following projects (updated at the time of writing this article, which may eventually undergo changes in their details):

LoRa Library for ESP-IDF: [https://github.com/JN513/esp32-lora-library](https://github.com/JN513/esp32-lora-library)

Library for LoRa Module usage via UART for ESP-IDF: [https://github.com/JN513/esp32_uart_sx1276](https://github.com/JN513/esp32_uart_sx1276)

LoRaWAN Gateway based on ESP32: [https://github.com/JN513/lora_gateway](https://github.com/JN513/lora_gateway)

LoRa Mesh Implementation: [https://github.com/JN513/Lora_Mesh_Network](https://github.com/JN513/Lora_Mesh_Network)

Raspberry PI based Gateway: [https://github.com/tftelkamp/single_chan_pkt_fwd](https://github.com/tftelkamp/single_chan_pkt_fwd)

Raspberry PI based Gateway: [https://github.com/ch2i/LoraGW-Setup](https://github.com/ch2i/LoraGW-Setup)

LoRaWAN Gateway based on ESP32: [https://github.com/sparkfun/ESP32_LoRa_1Ch_Gateway](https://github.com/sparkfun/ESP32_LoRa_1Ch_Gateway)


## Sources:

EMBARCADOS. Conheça a tecnologia LoRa® e o protocolo LoRaWAN. Available at: https://embarcados.com.br/conheca-tecnologia-lora-e-o-protocolo-lorawan/ Accessed on: 04/04/2023

Bertamoni, N. R. . Avaliação do esquema de modulação LoRa implementado em GNURadio e sintetizado em SDR. Available at: https://www.lume.ufrgs.br/bitstream/handle/10183/235783/001136770.pdf?sequence=1 Accessed on: 07/04/2023

UFRJ. Arquitetura da Rede. Available at: https://www.gta.ufrj.br/ensino/eel878/redes1-2019-1/vf/lora/arquitetura.html Accessed on: 07/04/2023

ELETROGATE. Monitoramento de Temperatura com Heltec ESP32 LoRa. Available at: https://blog.eletrogate.com/monitoramento-remoto-de-temperatura