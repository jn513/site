---
title: "Home"
date: 2023-04-27
draft: false
---

{{< most-recent-box "posts" "Check out our latest blog post!" >}}

{{< alerts info
"**Não fala inglês**? [Clique aqui](/pt-br)" 
>}}

# Welcome to Bzóide

[Bzóide](https://bzoide.dev/posts/genesis/) is a technology blog focused on developing content and projects related to electronics, embedded systems, IoT, security, and other related fields. We are open to any type of technology-related content.

# Who we are

We are a group of students from [UNICAMP](https://www.unicamp.br/unicamp/), passionate about technology and with extensive experience in electronics, programming, embedded systems, and IoT. Our goal is to share knowledge, develop open-source projects, and help people solve problems in their daily lives. That's why we decided to create this blog to share our experiences and help others learn new skills.

# Objectives

Our main objectives are:

- Disseminate knowledge about electronics, embedded systems, IoT, security, and other technology-related topics;
- Develop and promote open-source projects in all of the above areas;
- Help people learn new content and solve everyday technology-related problems.

# UNICAMP student participation

Our blog is maintained and developed by students from [UNICAMP](https://www.unicamp.br/unicamp/), which allows us to have a strong connection with the university. This connection gives us access to a wide range of resources and knowledge, allowing us to develop high-quality content and projects.

# Entities related to the Blog

The creator of this blog is a member of two [UNICAMP](https://www.unicamp.br/unicamp/) entities: [LKCAMP](https://lkcamp.dev) and [EMBARCAÇÕES](https://gitlab.com/embarcacoes). Therefore, there may be collaboration and common content between these entities and Bzóide. This only enriches our knowledge and network of contacts even further.